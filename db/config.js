const cloudinary = require('cloudinary');
const paypal = require('paypal-rest-sdk');
const isProduction =  process.env.NODE_ENV === 'production';
const redis = require('redis');
const client = redis.createClient();
const Rollbar = require("rollbar");


const rollbar = new Rollbar({
    accessToken: '4c2bf438e7864fc68106f1e065017153',
    captureUncaught: true,
    captureUnhandledRejections: true
});

cloudinary.config({ 
    cloud_name: 'streetwear-boutiques', 
    api_key: '325828734377849', 
    api_secret: 'FesaBmnOeizmI1KVOyChhugVjLE' 
});

const sendgrid = {
    "key": "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",
    "email": "streetwearboutiques@gmail.com",
    "subscribe_list" : "f927bae03a",
    "api_key": "2608c95c593c49fb9c3fe981630112c3-us17",
    "mailchimpInstance": "us17"

}

const paypalKeys = {
    "sandbox": {
        "client_id": "AVBubSe8JWx0VCQ_ngq3XXuN584uhLYCRUvm7Q4slkM454Snia7KFTWwGevdkr5KKpHnGxozsF9xw2tY",
        "client_secret": "EO3hgmGhkqw73Tn4UIXUJGr67tyrKo89AD9Dyc8K6SxdsV6GWYh-bVjM3aYb90NZjwC1wSjklaxFsBuJ"
    },
    "live": {
        "client_id": "AVxoIZpb0MiPZ_qCpiF9lmh5sfCV3Iw3Jr86S7OtL9ahDWYzq9M6nRYbFUQFAdlCEZNWyS_pjYLXqa3g",
        "client_secret": "EJclVtDUkRaQUz-Er904qtI4U9aju2MCdulHmE7SMEgfJxY5yYRtLUJob9_Vnmw9aGnBEp8EpN-zbc7x"
    },
    "redirect_urls": {
        "return_url": `${isProduction ? 'https://streetwearboutiques.com' : 'http://localhost:3000'}/profile/process`,
        "cancel_url": `${isProduction ? 'https://streetwearboutiques.com' : 'http://localhost:3000'}/profile/`,
    }
}

paypal.configure({
    mode: isProduction ? 'live' : 'sandbox',
    client_id: paypalKeys[isProduction ? "live" : "sandbox"].client_id,
    client_secret: paypalKeys[isProduction ? "live" : "sandbox"].client_secret,
})



client.on('connect', function() {
    console.log('Redis client connected');
});

client.on('error', function (err) {
    console.log('Something went wrong ' + err);
});

module.exports = {
    cloudinary,
    sendgrid,
    paypal,
    isProduction,
    paypalKeys,
    client,
    redis,
    rollbar
}