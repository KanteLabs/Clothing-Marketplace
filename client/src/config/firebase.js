import * as firebase from 'firebase';
// Required for side-effects
require('firebase/firestore');

const settings = {/* your settings... */ timestampsInSnapshots: true};

// Initialize Firebase
var config = {
    apiKey: 'AIzaSyC76udYV-SsFgyCI4mUSEMi1RXQpMG6VyI',
    authDomain: 'copped-9a558.firebaseapp.com',
    databaseURL: 'https://copped-9a558.firebaseio.com',
    projectId: 'copped-9a558',
    storageBucket: 'gs://copped-9a558.appspot.com/',
    messagingSenderId: '1094268024837',
};

firebase.initializeApp(config);
firebase.firestore().settings(settings);
firebase.firestore().enablePersistence();

export default firebase;
